


#ifndef __MULTI_PROCESSING_OBJECT_INTERFACE_H_
#define __MULTI_PROCESSING_OBJECT_INTERFACE_H_

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <iterator>

using namespace std;

class mpoi{
  
  
  
public:
  enum buffer_property{
    READ_ONLY= CL_MEM_READ_ONLY,
    WRITE_ONLY= CL_MEM_WRITE_ONLY,
    READ_WRITE= CL_MEM_READ_WRITE
  };
  
  
  
  
  
  
  
protected:
  cl_device_id _device_id;
  cl_context _context;
  cl_command_queue _cmd_queue;
  cl_program _program;
  vector<cl_kernel> _kernels;
  map<size_t,cl_mem> _buffers;
  size_t _next_key;
  
  
  
  
  
  
  
  
  
  
private:
  void _setup_opencl();
  void _cleanup_opencl();
  
  
  
  
  
  
  
public:
  void build_program(const string&);
  size_t create_kernel(const string&);
  
  
  
  
  
  
  
public:
  mpoi();
  mpoi(const string&);
  mpoi(const mpoi&);
  virtual~mpoi();
  
  
  
  
  
  
  
public:
  void display_platform_info()const;
protected:
  void _display_platform_info(cl_platform_id,cl_platform_info,string)const;
  
  
  
  
  
  
  
public:
  size_t create_buffer(mpoi::buffer_property,const size_t);
  void release_buffer(const size_t);
  
  
  
  
  
  
  
public:
  void enqueue_write_buffer(const size_t,const size_t,const void*);
  void enqueue_read_buffer(const size_t,const size_t,void*);
  
  
  
  
  
  
  
public:
  void
  set_kernel_argument(const size_t,const size_t,
                      const size_t);
  
  void
  set_kernel_argument(const size_t,const size_t,const size_t,
                      void*);
  
  
  
  
  
  
  
public:
  void enqueue_data_parallel_kernel(const size_t,size_t,size_t);
  
  
  
  
  
  
  
};

#endif





