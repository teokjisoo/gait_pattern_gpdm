


#include "mpoi.h"




mpoi::mpoi()
:_next_key(0)
{
  _setup_opencl();
}

mpoi::mpoi(const string&src)
:_next_key(0)
{
  _setup_opencl();
  build_program(src);
}

mpoi::mpoi(const mpoi&src)
:_device_id(src._device_id),
_context(src._context),
_cmd_queue(src._cmd_queue),
_program(src._program),
_kernels(src._kernels),
_buffers(src._buffers),
_next_key(src._next_key)
{}

mpoi::~mpoi(){
  _cleanup_opencl();
}







void mpoi::_setup_opencl(){
  cl_uint num_platforms;
  cl_int err= clGetPlatformIDs(0,NULL,&num_platforms);
  if(err!=CL_SUCCESS||num_platforms<1){
    cerr<<"Failed to find any OpenCL platforms.\n";
    exit(1);
  }
  
  cl_platform_id*platformIDs
  = (cl_platform_id*)new cl_platform_id[num_platforms];
  err= clGetPlatformIDs(num_platforms,platformIDs,NULL);
  if(err!=CL_SUCCESS){
    cerr<<"Failed to find any OpenCL platforms.\n";
    exit(1);
  }
  cout<<num_platforms<<" OpenCL platform(s) found.\n";
  
  bool device_found= false;
  cl_uint current_max_compute_units= 0;
  cl_uint max_compute_units= 0;
  
  for(cl_uint i= 0;i!=num_platforms;i++){
    cout<<"Platform # "<<i<<endl;
    cl_uint num_devices;
    err= clGetDeviceIDs(platformIDs[i],CL_DEVICE_TYPE_GPU,
                        0,NULL,&num_devices);
    if(num_devices<1){
      cerr<<"No GPU devices found for platform "<<platformIDs[i]<<endl;
    }else{
      device_found= true;
      cout<<num_devices<<" GPU device(s) found for platform "
      <<platformIDs[i]<<endl;
      cl_device_id*deviceIDs= (cl_device_id*)new cl_device_id[num_devices];
      for(cl_uint j= 0;j!=num_devices;j++){
        err= clGetDeviceIDs(platformIDs[i],CL_DEVICE_TYPE_GPU,1,
                            &deviceIDs[j],NULL);
        cl_uint device_vendor_id;
        err= clGetDeviceInfo(deviceIDs[j],CL_DEVICE_VENDOR_ID,
                             sizeof(cl_uint),&device_vendor_id,NULL);
        cout<<"Device vendor ID: "<<device_vendor_id<<endl;
        err= clGetDeviceInfo(deviceIDs[j],CL_DEVICE_MAX_COMPUTE_UNITS,
                             sizeof(cl_uint),&max_compute_units,NULL);
        cout<<"Device has "<<max_compute_units<<" compute units.\n";
        if(max_compute_units> current_max_compute_units){
          current_max_compute_units= max_compute_units;
          _device_id= deviceIDs[j];
        }
      }
      delete[]deviceIDs;
    }
  }
  
  if(!device_found){
    cerr<<"No OpenCL GPU devices found through all the platforms.\n";
    exit(1);
  }
  
  _context= clCreateContext(NULL,1,&_device_id,NULL,NULL,&err);
  if(err!=CL_SUCCESS){
    cerr<<"Error in creating a context.\n";
    exit(1);
  }
  _cmd_queue= clCreateCommandQueue(_context,_device_id,0,&err);
  if(err!=CL_SUCCESS){
    cerr<<"Error in creating a command queue.\n";
    exit(1);
  }
  _program= NULL;
  
  delete[]platformIDs;
}

void mpoi::_cleanup_opencl(){
  clFlush(_cmd_queue);
  clFinish(_cmd_queue);
  for(size_t i= 0;i!=_kernels.size();i++){
    clReleaseKernel(_kernels[i]);
  }
  if(!_program){
    clReleaseProgram(_program);
  }
  clReleaseCommandQueue(_cmd_queue);
  clReleaseContext(_context);
}







void mpoi::build_program(const string&src_file){
  ifstream in(src_file);
  if(in.is_open()){
    string src((istreambuf_iterator<char> (in)),istreambuf_iterator<char> ());
    const char*src_string= src.c_str();
    const size_t src_length= src.length();
    cl_int err;
    
    _program= clCreateProgramWithSource(_context,1,
                                        (const char**)&src_string,
                                        (const size_t*)&src_length,
                                        &err);
    if(err!=CL_SUCCESS){
      cerr<<"Error in creating a program.\n";
    }
    
    err= clBuildProgram(_program,1,&_device_id,NULL,NULL,NULL);
    
    
    
    if(err!=CL_SUCCESS){
      cerr<<"Error in building a program.\n";
      cl_build_status build_status;
      
      clGetProgramBuildInfo(_program,_device_id,
                            CL_PROGRAM_BUILD_STATUS,
                            sizeof(cl_build_status),
                            &build_status,NULL);
      
      if(build_status!=CL_SUCCESS){
        size_t ret_val_size;
        clGetProgramBuildInfo(_program,_device_id,
                              CL_PROGRAM_BUILD_LOG,0,
                              NULL,&ret_val_size);
        
        char*build_log= (char*)new char[ret_val_size+1];
        
        clGetProgramBuildInfo(_program,_device_id,
                              CL_PROGRAM_BUILD_LOG,ret_val_size,
                              build_log,NULL);
        
        build_log[ret_val_size]= '\0';
        
        cerr<<"BUILD LOG: "<<build_log<<endl;
        
        delete[]build_log;
      }
    }
    
    
    
    
    
    
    ;
    
  }else{
    exit(1);
  }
}

size_t mpoi::create_kernel(const string&name){
  size_t id= _kernels.size();
  cl_int err;
  _kernels.push_back(clCreateKernel(_program,name.c_str(),&err));
  return id;
}







void mpoi::display_platform_info()const{
  cl_uint num_platforms;
  cl_int err= clGetPlatformIDs(0,NULL,&num_platforms);
  if(err!=CL_SUCCESS||num_platforms<1){
    cerr<<"Failed to find any OpenCL platforms.\n";
    return;
  }
  
  cl_platform_id*platformIDs
  = (cl_platform_id*)new cl_platform_id[num_platforms];
  
  err= clGetPlatformIDs(num_platforms,platformIDs,NULL);
  if(err!=CL_SUCCESS){
    cerr<<"Failed to find any OpenCL platforms.\n";
    return;
  }
  cout<<"Number of platforms: \t"<<num_platforms<<endl;
  
  for(cl_uint i= 0;i!=num_platforms;i++){
    _display_platform_info(platformIDs[i],CL_PLATFORM_PROFILE,
                           "CL_PLATFORM_PROFILE   ");
    _display_platform_info(platformIDs[i],CL_PLATFORM_VERSION,
                           "CL_PLATFORM_VERSION   ");
    _display_platform_info(platformIDs[i],CL_PLATFORM_VENDOR,
                           "CL_PLATFORM_VENDOR    ");
    _display_platform_info(platformIDs[i],CL_PLATFORM_EXTENSIONS,
                           "CL_PLATFORM_EXTENSIONS");
  }
  
  delete[]platformIDs;
}

void
mpoi::_display_platform_info(cl_platform_id id,
                             cl_platform_info name,
                             string str
                             )const{
  size_t param_value_size;
  cl_int err= clGetPlatformInfo(id,name,0,NULL,&param_value_size);
  if(err!=CL_SUCCESS){
    cerr<<"Failed to find OpenCL platform "<<str.c_str()<<".\n";
    return;
  }
  
  char*info= (char*)new char[param_value_size];
  err= clGetPlatformInfo(id,name,param_value_size,info,NULL);
  if(err!=CL_SUCCESS){
    cerr<<"Failed to find OpenCL platform "<<str.c_str()<<".\n";
    return;
  }
  cout<<"\t"<<str.c_str()<<":\t"<<info<<endl;
  
  delete[]info;
}







size_t mpoi::create_buffer(mpoi::buffer_property bp,
                           const size_t sz
                           ){
  
  cl_int err;
  cl_mem buffer= clCreateBuffer(_context,bp,sz,NULL,&err);
  _buffers[_next_key]= buffer;
  _next_key++;
  return _next_key-1;
}

void mpoi::release_buffer(const size_t id){
  if(_buffers[id]!=NULL){
    clReleaseMemObject(_buffers[id]);
    _buffers[id]= NULL;
  }
}




void mpoi::enqueue_write_buffer(const size_t id,
                                const size_t size,
                                const void*mem
                                ){
  if(_buffers[id]!=NULL){
    cl_int err= clEnqueueWriteBuffer(_cmd_queue,
                                     _buffers[id],
                                     CL_TRUE,0,size,mem,
                                     0,NULL,NULL);
    if(err!=CL_SUCCESS){
      cerr<<"Error in enqueuing a write buffer.\n";
      return;
    }
  }
}

void mpoi::enqueue_read_buffer(const size_t id,
                               const size_t size,
                               void*mem
                               ){
  if(_buffers[id]!=NULL){
    cl_int err= clEnqueueReadBuffer(_cmd_queue,
                                    _buffers[id],
                                    CL_TRUE,0,size,mem,
                                    0,NULL,NULL);
    if(err!=CL_SUCCESS){
      cerr<<"Error in enqueuing a read buffer.\n";
      return;
    }
  }
}







void mpoi::set_kernel_argument(const size_t kernel_id,
                               const size_t order,
                               const size_t buffer_id
                               ){
  if((_kernels[kernel_id]!=NULL)&&(_buffers[buffer_id]!=NULL)){
    cl_int err= clSetKernelArg(_kernels[kernel_id],
                               static_cast<cl_uint> (order),sizeof(cl_mem),
                               (void*)&(_buffers[buffer_id]));
    if(err!=CL_SUCCESS){
      cerr<<"Error in setting a kernel argument!\n";
      return;
    }
  }
}

void mpoi::set_kernel_argument(const size_t id,
                               const size_t order,
                               const size_t size,
                               void*mem
                               ){
  if(_kernels[id]!=NULL){
    cl_int err= clSetKernelArg(_kernels[id],
                               static_cast<cl_uint> (order),size,mem);
    
    if(err!=CL_SUCCESS){
      cerr<<"Error in setting a kernel argument!\n";
      return;
    }
  }
}







void mpoi::enqueue_data_parallel_kernel(const size_t id,
                                        size_t num_global_items,
                                        size_t num_local_items
                                        ){
  if(_kernels[id]!=NULL){
    for(;num_local_items!=1;num_local_items--){
      if(num_global_items%num_local_items==0)break;
    }
    cout<<"Local item size = "<<num_local_items<<endl;
    cl_int err= clEnqueueNDRangeKernel(_cmd_queue,
                                       _kernels[id],
                                       1,NULL,
                                       &num_global_items,
                                       &num_local_items,
                                       0,NULL,NULL);
    if(err!=CL_SUCCESS){
      cerr<<"Error in enqueuing nd range kernel.\n";
      return;
    }
  }
}









