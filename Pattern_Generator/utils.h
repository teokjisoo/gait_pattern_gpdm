#pragma once


#include <fstream>
#include <string>
#include <vector>

#include "CMatrix.h"

using namespace std;

typedef vector<double>						vec;
typedef vector< vector<double> >			array2d;
typedef vector< vector< vector<double> > >	array3d;

namespace utils
{
	void exportToFile(const array2d& mat, const string& fileName)
	{
		ofstream outText(fileName);
		for (size_t row = 0; row < mat.size(); row++)
		{
			for (size_t col = 0; col < mat[row].size(); col++)
				outText << mat[row][col] << '\t';
			outText << '\n';
		}
		outText.close();
	}

	void exportToFile(const CMatrix& mat, const string& fileName)
	{
		ofstream outText(fileName);
		for (size_t row = 0; row < mat.getRows(); row++)
		{
			for (size_t col = 0; col < mat.getCols(); col++)
				outText << mat.getVal(row, col) << '\t';
			outText << '\n';
		}
		outText.close();
	}
}