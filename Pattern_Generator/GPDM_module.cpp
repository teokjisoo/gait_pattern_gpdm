#include "GPDM_module.h"

GPDM_module::GPDM_module()
{
}

GPDM_module::~GPDM_module()
{
}

CMatrix GPDM_module::mapping(const CMatrix & _state) const
{
	CMatrix result = multiply(_kernel(m_X, _state, m_theta, RBF), "t", m_invK, "t");
	result = multiply(result, m_Yc);
	for (int r = 0; r < result.getRows(); r++)
		for (int c = 0; c < result.getCols(); c++)
			result.addVal(m_Ymean.getVal(c), r, c);

	return result;
}

void GPDM_module::setLatentCoordinate(const CMatrix & _X)
{
	m_X = _X;
	m_latentDim = _X.getCols();
	m_numData = _X.getRows();
	return;
}

void GPDM_module::setObservation(const CMatrix & _Yc, const CMatrix & _Ymean)
{
	DIMENSIONMATCH(_Yc.getCols() == _Ymean.getCols());
	m_Yc = _Yc;
	m_Ymean = _Ymean;
	m_obsrvDim = _Yc.getCols();
	return;
}

void GPDM_module::setParameters(const CMatrix & _theta, const CMatrix & _thetad, const CMatrix & _w)
{
	m_theta = _theta;
	m_thetad = _thetad;
	m_w = _w;
	return;
}

void GPDM_module::setOtherInfo(const vector<int>& _modelType, const vector<int>& _segments, int _periodFrames, int _duplicatedFrames)
{
	m_modelType = _modelType;
	m_segments = _segments;
	m_numFrame = _periodFrames;
	m_numDuplicatedFrame = _duplicatedFrames;
	return;
}

void GPDM_module::initModel()
{
	auto mapKernels = _computeKernel(m_X, m_theta, RBF);
	auto X_in_out = _seperateXInOut();
	m_Xin = X_in_out.first;
	m_Xout = X_in_out.second;
	auto dynKernels = _computeKernel(m_Xin, m_thetad, (kernelType)m_modelType[2]);

	m_K = mapKernels.first;
	m_invK = mapKernels.second;
	m_Kd = dynKernels.first;
	m_invKd = dynKernels.second;

	return;
}

CMatrix GPDM_module::_kernel(const CMatrix & _X1, const CMatrix & _X2, const CMatrix & _theta, kernelType _type) const
{
	CMatrix kx(_X1.getRows(), _X2.getRows());
	switch (_type)
	{
	case RBF:
		for (int i = 0; i < _X1.getRows(); i++)
			for (int j = 0; j < _X2.getRows(); j++)
				kx.setVal(_X1.dist2Row(i, _X2, j), i, j);
		kx.scale(-_theta.getVal(0) / 2.0);
		kx.exp();
		kx.scale(_theta.getVal(1));
		break;
	case Lin:
		kx.gemm(_X1, _X2, _theta.getVal(0), 0.0, "n", "t");
		break;
	case RBF_Lin:
		for (int i = 0; i < _X1.getRows(); i++)
			for (int j = 0; j < _X2.getRows(); j++)
				kx.setVal(_X1.dist2Row(i, _X2, j), i, j);
		kx.scale(-_theta.getVal(1) / 2.0);
		kx.exp();
		kx.scale(_theta.getVal(2));
		kx.gemm(_X1, _X2, _theta.getVal(0), 1.0, "n", "t");
		break;

	case RBF_w:		//	TODO : implement weighted RBF kernel
	case Lin_w:		//	TODO : implement weighted linear kernel
	case RBF_Lin_w:	//	TODO : implement weighted RBF+linear kernel
	default:
		SANITYCHECK(false);
		break;
	}
	return kx;
}

CMatrixPair GPDM_module::_computeKernel(const CMatrix & _X, const CMatrix & _theta, kernelType _type) const
{
	CMatrix K(_kernel(_X, _X, _theta, _type));
	K.addDiag(1.0 / _theta.getVal(_theta.getNumElements() - 1));
	CMatrix invK(K);
	invK.inv();
	return make_pair(K, invK);
}

CMatrixPair GPDM_module::_computeKernel(const CMatrix & _X, const CMatrix & _theta, kernelType _type, const CMatrix & _Kn, const CMatrix & _invKn) const
{
	int n = _Kn.getRows();		//	number of element of which kernel was pre-calculated
	int N = _X.getRows();		//	number of total elements
	int d = _X.getCols();		//	dimension of x

	CMatrix X1(n, d);
	CMatrix X2(N - n, d);

	//	merged kernel
	_X.getMatrix(X1, 0, n - 1, 0, d - 1);
	_X.getMatrix(X2, n, N - 1, 0, d - 1);

	CMatrix Kb = _kernel(X1, X2, _theta, _type);
	CMatrix Kc = _kernel(X2, X2, _theta, _type);
	Kc.addDiag(1.0 / _theta.getVal(_theta.getNumElements() - 1));
	CMatrix KbT(Kb); KbT.trans();

	CMatrix K(N, N), invK(N, N);
	K.setMatrix(0, 0, _Kn);
	K.setMatrix(0, n, Kb);
	K.setMatrix(n, 0, KbT);
	K.setMatrix(n, n, Kc);

	//	inverse of merged Kernel
	CMatrix invKd(Kc);
	CMatrix calcTemp(N - n, n);
	calcTemp.gemm(Kb, _invKn, 1.0, 0.0, "t", "n");
	Kc.gemm(calcTemp, Kb, -1.0, 1.0, "n", "n");		//	D
	Kc.inv();										//	invD

	//	invKa
	calcTemp = multiply(_invKn, Kb);
	calcTemp = multiply(calcTemp, Kc);
	calcTemp = multiply(calcTemp, KbT);
	calcTemp = multiply(calcTemp, _invKn);
	calcTemp.add(_invKn);
	invK.setMatrix(0, 0, calcTemp);
	//	invKb
	calcTemp.resize(n, N - n);
	calcTemp.gemm(_invKn, Kb, -1.0, 0.0, "n", "n");
	calcTemp = multiply(calcTemp, Kc);
	invK.setMatrix(0, n, calcTemp);
	//	invKc
	calcTemp.trans();
	invK.setMatrix(n, 0, calcTemp);
	//	m_invKd
	invK.setMatrix(n, n, Kc);

	return make_pair(K, invK);
}

CMatrixPair GPDM_module::_seperateXInOut()
{
	return _seperateXInOut(m_X, m_segments);
}

CMatrixPair GPDM_module::_seperateXInOut(const CMatrix& _X, const vector<int>& _segments)
{
	int q = _X.getCols();
	int n_frame;
	CMatrix Xin, Xout;
	switch (m_modelType[0])
	{
	case 0:
		n_frame = m_numFrame - 2;
		Xin.resize(n_frame*_segments.size(), 2 * q);
		for (unsigned s = 0; s < _segments.size(); s++)
			//	left side of Xin : second state of each segments
			//	right side of Xin : first state of each segments, 
			for (int r = 0; r < n_frame; r++)
				for (int c = 0; c < 2 * q; c++)
					Xin.setVal(_X.getVal(_segments[s] + r - c / q, c%q), _segments[s] - 2 * s - 1 + r, c);
		break;
		//	TODO : implement other cases
		SANITYCHECK(false);
	}

	switch (m_modelType[1])
	{
	case 0:
		if (m_modelType[0] == 0 || m_modelType[0] == 1)
		{
			n_frame = m_numFrame - 2;
			Xout.resize(n_frame*_segments.size(), q);
			for (unsigned s = 0; s < _segments.size(); s++)
				for (int r = 0; r < n_frame; r++)
					Xout.copyRowRow(_segments[s] - 2 * s - 1 + r, _X, _segments[s] + r + 1);
		}
		else
			SANITYCHECK(false);
		break;
	default:
		//	TODO : implement other cases
		SANITYCHECK(false);
		break;

	}
	return make_pair(Xin, Xout);
}

CMatrix GPDM_module::simulateDynamics(const CMatrix & _state, int _totalSteps) const
{
	int dynOrder = (m_modelType[0] == 2) ? 1 : 2;
	DIMENSIONMATCH(_state.getRows() == dynOrder);
	DIMENSIONMATCH(_state.getCols() == m_latentDim);
	CHECKZEROORPOSITIVE(_totalSteps >= _state.getRows());


	CMatrix		simResult(_totalSteps, m_latentDim), dynInput(dynOrder, m_latentDim);
	simResult.setMatrix(0, 0, _state);
	CMatrix		nextState;
	for (int k = dynOrder; k < _totalSteps; k++)
	{
		//TODO : implement simulation
		if (dynOrder == 1)
			dynInput.copyRowRow(0, simResult, k - 1);
		else
		{
			dynInput.copyRowRow(0, simResult, k - 2);
			dynInput.copyRowRow(1, simResult, k - 1);
		}
		nextState = _manifoldOutputs(dynInput, m_Xin, m_Xout, m_thetad, m_invKd, m_modelType);
		simResult.copyRowRow(k, nextState, 0);
	}

	return simResult;
}

CMatrix GPDM_module::_manifoldOutputs(const CMatrix& _input, const CMatrix& _Xin, const CMatrix& _Xout, const CMatrix& _thetad, const CMatrix& _invKd, const vector<int>& _modelType) const
{
	//	_input : dynOrder by q matrix.
	CMatrix	x0;
	int dynOrder = (m_modelType[0] == 2) ? 1 : 2;
	DIMENSIONMATCH(_input.getRows() == dynOrder);

	switch (_modelType[0])
	{
	case 0:
		x0.resize(1, 2 * m_latentDim);
		for (int i = 0; i < m_latentDim; i++)
		{
			x0.setVal(_input.getVal(1, i), i);
			x0.setVal(_input.getVal(0, i), i + m_latentDim);
		}
		break;
	case 1:
		x0.resize(1, 2 * m_latentDim);
		for (int i = 0; i < m_latentDim; i++)
		{
			x0.setVal(_input.getVal(1, i), i);
			x0.setVal(_input.getVal(1, i) - _input.getVal(0, i), i + m_latentDim);
		}
		break;
	case 2:
		x0 = _input;
		break;
	default:
		break;
	}

	CMatrix result = multiply(multiply(_kernel(x0, _Xin, _thetad, (kernelType)_modelType[2]), "n", _invKd, "t"), _Xout);

	if (_modelType[1] == 1)
		for (int i = 0; i < m_latentDim; i++)
			result.addVal(x0.getVal(i), i);

	return result;

	//switch (_type)
	//{
	//case RBF_w:
	//	SANITYCHECK(false);
	//	break;
	//case RBF:
	//	break;
	//case Lin:
	//	break;
	//case RBF_Lin_w:
	//	SANITYCHECK(false);
	//	break;
	//case Lin_w:
	//	SANITYCHECK(false);
	//	break;
	//case RBF_Lin:
	//	break;
	//default:
	//	SANITYCHECK(false);
	//	break;
	//}
}



