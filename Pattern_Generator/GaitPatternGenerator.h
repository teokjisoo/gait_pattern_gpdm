#pragma once

#include <iostream>
#include <string>
#include <vector>
//#include <cmath>
#include "CMatrix.h"
#include "CGp.h"

#include "GPDM_module.h"
#include "3rd_party\cagd.h"
#include "srLibMath\LieGroup.h"
#include "vectorOp.hpp"

using namespace std;
using namespace vectorOp;
using namespace cagd;
typedef std::vector<double>									vec;
typedef std::vector< std::vector<double> >					array2d;
typedef std::vector< std::vector< std::vector<double> > >	array3d;
typedef std::vector<SE3>									vecSE3;

class GaitPatternGenerator
{
public:
	GaitPatternGenerator(const string & _modelFileName, const string& _featureFileName);
	virtual ~GaitPatternGenerator();

	enum class featureIndex
	{
		AGE = 0,		//	years
		HEIGHT,			//	cm
		MASS,			//	kg
		THIGH,			//	cm
		CALF,			//	cm
		BI_TROCH,		//	cm
		BI_ILIAC,		//	cm
		ASIS,			//	cm
		KNEE,			//	cm
		FOOT_L,			//	cm
		MALLEOLUS_H,	//	cm
		MALLEOLUS_W,	//	cm
		FOOT_B,			//	cm
		SPEED,			//	km/h
		NUM_OF_FEATURES
	};

	enum class JOINT_LABEL
	{
		PELVIS_X = 0,
		PELVIS_Y,
		PELVIS_Z,
		PELVIS_YAW,


		R_HIP_ROLL,
		R_HIP_PITCH,
		R_HIP_YAW,
		R_KNEE_PITCH,
		R_ANKLE_PITCH,

		L_HIP_ROLL,		//	== NUM_OF_RIGHT_JOINTS
		L_HIP_PITCH,
		L_HIP_YAW,
		L_KNEE_PITCH,
		L_ANKLE_PITCH,

		NUM_OF_JOINTS
	};

	enum class MARKER_LABEL
	{
		PELVIS = 0,

		R_HIP,
		R_KNEE,
		R_ANKLE,
		R_TOE,
		R_HEEL,

		L_HIP,
		L_KNEE,
		L_ANKLE,
		L_TOE,
		L_HEEL,

		NUM_OF_MARKERS
	};

	enum class LINK_LABEL
	{
		PELV_TO_HIP = 0,
		HIP_TO_KNEE,
		KNEE_TO_ANK,
		ANK_TO_GRD,
		FOOT_LENGTH,

		NUM_OF_LINK_LENGTH
	};

	enum class MODIFICATION_METHOD
	{
		HOLD_ANKLE_ANGLE = 0,
		HOLD_INCIDENCE_ANGLE
	};

	//	
	//	run "updateTrajectory" after set feature. (recommended)
	void	setFeature(double _val, featureIndex _idx);
	void	setFeature(vector<double>& _vals);
	void	setFeature(const CMatrix& _vals);
	//	set desired minimum distance between foot and ground in (cm) unit.
	void	setClearance(double _z);		//	_z : (cm)
	void	enableCollisionAvoidance(bool _on = true);
	void	setCollisionAvoidanceScheme(MODIFICATION_METHOD _scheme);
	//	update(estimate) joint trajectory based on body feature.
	void	updateTrajectory();

	void	exportTraj(const string& fileName, double timeStep = 0.001);

	//	output is measured in degree. order follows JOINT_LABEL.
	vector<double>	queryPose(double _t, bool isTimeNormalized = true);


//	protected:
	vecSE3	_forwardKinematics(const vec& _conf) const;
	void	_updateCollisionInfo(const cagd::cubic_spline& _traj);
	vec		_query_dq(double _s) const;
	vec		_calc_dq(const vecSE3& _collisionPose, double _dz, MODIFICATION_METHOD _mode = MODIFICATION_METHOD::HOLD_ANKLE_ANGLE);
	vec		_inverseKinematics(double _px, double _pz, double _l1, double _l2);

	//	patient specific vars
	CMatrix				m_features;
	CMatrix				m_X0;
	cagd::cubic_spline	m_trajSpline;
	//	utility
	bool				m_isTrajUptodate;
	//	GPDM related variables.
	GPDM_module			m_GPDM;

	//	Gaussian Process related variables.
	CGp*				m_GP;
	CMatrix				m_gp_x, m_gp_y;
	CCmpndKern*			m_cmpndKern;
	vector<CKern*>		m_kernels;
	CGaussianNoise*		m_noise;

	//	Variables for collision handling
	MODIFICATION_METHOD	m_colAvoidScheme;
	bool	m_collisionHandling;				//	turn on/off collision avoidance feature.
	double	m_clearance;						//	user defined minimum distance between ground and sole
	double	m_ZoffsetPelvis;					//	offset of PELVIS_Z
	double	m_collisionPhase;					//	gait phase at collision time
	double	m_collisionZtoe;					//	z-axis value of toe at collision time
	vecSE3	m_collisionPose;					//	SE3 pose at collision time
	vec		m_collisionConf;					//	joint configuration at collision time
	vec		m_dq;								//	additional configuration to avoid collision		
};