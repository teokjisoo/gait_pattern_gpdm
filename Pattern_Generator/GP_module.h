#pragma once

#include <iostream>
#include <string>
#include "CMatrix.h"
#include "CGp.h"

/*
GP_module :
Gaussian Process 를 구성하는 data와 hyper parameters, 그리고 mapping 계산 함수들을 포함하는 class.
*/


class GP_module
{
public:
	GP_module();
	virtual ~GP_module();


	CGp		m_GP;
	CMatrix X, Y, theta_cov, theta_lik;
};