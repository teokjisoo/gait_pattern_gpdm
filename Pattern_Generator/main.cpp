#ifndef FOR_VS2015
#include <iostream>
// added by JS
static FILE _iob[] = { *stdin, *stdout, *stderr };
extern "C" FILE * __cdecl __iob_func(void) { return _iob; }
#endif // !FOR_VS2015

#include <iostream>
#include "GaitPatternGenerator.h"
#include "utils.h"


int main()
{
	GaitPatternGenerator generator("../data/GPDM_result_male.mat", "../data/features_male.mat");

	vector<double>	features((unsigned)GaitPatternGenerator::featureIndex::NUM_OF_FEATURES);

	//	set patient's body feature.
	//	unsettled features are initialized as mean of training data .
	generator.setFeature(30, GaitPatternGenerator::featureIndex::AGE);
	generator.setFeature(184, GaitPatternGenerator::featureIndex::HEIGHT);
	generator.setFeature(80, GaitPatternGenerator::featureIndex::MASS);
	generator.setFeature(3, GaitPatternGenerator::featureIndex::SPEED);
	//generator.setFeature(50, GaitPatternGenerator::featureIndex::THIGH);

	//	it is recommended that call 'updateTrajectory' after set features up.
	generator.updateTrajectory();

	//	set desired minimum distance between ground and foot in (cm) unit.
	generator.setClearance(3.0);

	//	set collision avoidance scheme (it's default scheme).
	generator.setCollisionAvoidanceScheme(GaitPatternGenerator::MODIFICATION_METHOD::HOLD_ANKLE_ANGLE);

	//	export trajectory without collision avoidance process.
	generator.enableCollisionAvoidance(false);
	generator.exportTraj("../data/traj.txt", 0.01);

	//	export trajectory with collision avoidance process.
	generator.enableCollisionAvoidance(true);
	generator.exportTraj("../data/traj_mod.txt", 0.01);

	//	export feature vector.
	utils::exportToFile(generator.m_features, "../data/features.txt");

	//	to notify end of program.
	cout << "exit program" << endl;
	return 0;
}
