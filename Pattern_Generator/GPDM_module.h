#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include "CMatrix.h"

/*
	GPDM_module :
	Gaussian Process Dynamical Model을 구성하는
	data와 hyper parameters, 그리고 dynamics 및 mapping 계산 함수들을 포함하는 class.
*/

using namespace std;
typedef pair<CMatrix, CMatrix> CMatrixPair;
class GPDM_module
{
public:
	GPDM_module();
	virtual ~GPDM_module();


	//	Simulate dynamics from given state.
	//	_state		: initial state (in each column).
	//	_totalSteps	: total number of steps to simulate (include given state).
	//	return		: simulated next (_numSteps) states based on learned dynamic model.
	CMatrix				simulateDynamics(const CMatrix& _state, int _totalSteps) const;

	//	Map latent coordinates to observation space.
	//	_state		: coordinates in latent space.
	//	return		: mapped coordinates in observation space.
	CMatrix				mapping(const CMatrix& _state) const;

	//	Set model information	(please set all these variables before running)
	void setLatentCoordinate(const CMatrix& _X);
	void setObservation(const CMatrix& _Yc, const CMatrix& _Ymean);
	void setParameters(const CMatrix& _theta, const CMatrix& _thetad, const CMatrix& _w);
	void setOtherInfo(const vector<int>& _modelType, const vector<int>& _segments, int _periodFrames, int _duplicatedFrames);
	int	getDynOrder() const { return (m_modelType[0] == 0) ? 2 : 1; }
	//	initialization after variable setting up. set model information before run 'initModel'
	void initModel();


	//	private functions
	enum kernelType
	{
		RBF_w = 0,	//	RBF kernel with weighted dimensions
		RBF,		//	RBF kernel
		Lin,		//	Linear kernel
		RBF_Lin_w,	//	weighted Linear kernel + RBF kernel with weighted dimensions
		Lin_w,		//	weighted linear kernel
		RBF_Lin		//	RBF + linear
	};
	//	Compute the kernel with _X1 & _X2
	CMatrix				_kernel(const CMatrix& _X1, const CMatrix& _X2, const CMatrix& _theta, kernelType _type) const;
	//	Compute the kernel and its inverse with _X 
	CMatrixPair			_computeKernel(const CMatrix& _X, const CMatrix& _theta, kernelType _type) const;
	CMatrixPair			_computeKernel(const CMatrix& _X, const CMatrix& _theta, kernelType _type, const CMatrix& _Kn, const CMatrix& _invKn) const;
	CMatrixPair			_seperateXInOut();
	CMatrixPair			_seperateXInOut(const CMatrix& _X, const vector<int>& _segments);

	CMatrix				_manifoldOutputs(const CMatrix& _input, const CMatrix& _Xin, const CMatrix& _Xout, const CMatrix& _thetad, const CMatrix& _invKd, const vector<int>& _modelType) const; 

	//	X			: learned latent coordinates corresponding to observed data.
	//	Yc			: centred observation data.
	//	Ymean		: mean values of observed data.
	//	theta		: hyper-parameters of mapping kernel.
	//	thetad		: hyper-parameters of dynamics kernel.
	//	w			: hyper-parameter which affects scale of each dimension.
	//	modelType	: Define type of model (dim. of dynamics, etc).
	//	segments	: indices of frame which indicate beginning of each sequence.
	//	numFrame	: number of frames in single period (= 11 in my matlab implementation).
	//	numDuplicatedFrame : number of frames which are duplicated in end of sequence to make trajectory periodic.
	CMatrix			m_X, m_Yc, m_Ymean, m_theta, m_thetad, m_w;
	CMatrix			m_Xin, m_Xout, m_K, m_invK, m_Kd, m_invKd;
	vector<int>		m_modelType, m_segments;
	int				m_numFrame, m_numDuplicatedFrame;

	int				m_latentDim, m_obsrvDim, m_numData;
};