#include "GaitPatternGenerator.h"

GaitPatternGenerator::GaitPatternGenerator (const string & _modelFileName,
                                            const string& _featureFileName
                                            )
: m_features(1, (unsigned)featureIndex::NUM_OF_FEATURES),
m_isTrajUptodate(false),
m_clearance(0.0),
m_collisionHandling(false),
m_ZoffsetPelvis(0.0),
m_collisionPhase(0.0),
m_collisionZtoe(0.0),
m_colAvoidScheme(MODIFICATION_METHOD::HOLD_ANKLE_ANGLE),
m_trajSpline (cubic_spline(vector<point>(1,point(1))))
{
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //	Import pre-trained GPDM from .mat file.
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //	X			: learned latent coordinates corresponding to observed data.
  //	Ymean		: mean values of observed data.
  //	theta		: hyper-parameters of mapping kernel.
  //	thetad		: hyper-parameters of dynamics kernel.
  //	w			: hyper-parameter which affects scascaleil of each dimension.
  //	modelType	: Define type of model (dim. of dynamics, etc).
  //	segments	: indices of frame which indicate beginning of each sequence.
  //	numFrame	: number of frames in single period (= 11 in my matlab implementation).
  //	numDuplicatedFrame : number of frames which are duplicated in end of sequence to make trajectory periodic.
  
  CMatrix			X, Yc, Ymean, theta, thetad, w, tempCMat;
  vector<int>		modelType, segments;
  int				numFrame, numDuplicatedFrame;
  
  X.readMatlabFile(_modelFileName, "X");
  Yc.readMatlabFile(_modelFileName, "Y_n");
  Ymean.readMatlabFile(_modelFileName, "Y_mean");
  theta.readMatlabFile(_modelFileName, "theta");
  thetad.readMatlabFile(_modelFileName, "thetad");
  w.readMatlabFile(_modelFileName, "w");
  
  tempCMat.readMatlabFile(_modelFileName, "modelType");
  modelType.resize(tempCMat.getNumElements());
  for (unsigned i = 0; i < modelType.size(); i++)
    modelType[i] = round(tempCMat.getVal(i));
  
  tempCMat.readMatlabFile(_modelFileName, "segments");
  segments.resize(tempCMat.getNumElements());
  for (unsigned i = 0; i < segments.size(); i++)
    segments[i] = round(tempCMat.getVal(i));
  
  tempCMat.readMatlabFile(_modelFileName, "n_frame");
  numFrame = tempCMat.getVal(0);
  
  tempCMat.readMatlabFile(_modelFileName, "n_duplicated_frame");
  numDuplicatedFrame = tempCMat.getVal(0);
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //	Initialize GPDM model
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  m_GPDM.setLatentCoordinate(X);
  m_GPDM.setObservation(Yc, Ymean);
  m_GPDM.setParameters(theta, thetad, w);
  m_GPDM.setOtherInfo(modelType, segments, numFrame, numDuplicatedFrame);
  m_GPDM.initModel();
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //	Adapt features and latent coordinates for GP module.
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //	GP modules construction
  CMatrix	features;
  features.readMatlabFile(_featureFileName, "features");
  int dynOrder = (modelType[0] == 0) ? 2 : 1;
  int latentDim = X.getCols();
  
  
  //	featureø° ∫∏«‡º”µµ (1,2,3 km/h)∏¶ √ﬂ∞°«—¥Ÿ.
  int subjNum = features.getCols();
  CMatrix lastRow(1, subjNum * 3);
  m_gp_x.deepCopy(features);
  m_gp_x.appendCols(features);
  m_gp_x.appendCols(features);
  m_gp_x.appendRows(lastRow);
  for (int i = 0; i < m_gp_x.getCols(); i++)
    m_gp_x.setVal(i / subjNum + 1, m_gp_x.getRows() - 1, i);
  m_gp_x.trans();
  m_features = meanCol(m_gp_x);
  //cout << m_gp_x << endl;
  
  
  //	construct m_gp_y, which are target values of GP regression.
  m_gp_y.resize(segments.size(), dynOrder*latentDim);
  for (int i = 0; i < dynOrder; i++)
    for (int j = 0; j < latentDim; j++)
      for (unsigned k = 0; k < segments.size(); k++)
        m_gp_y.setVal(X.getVal(i + k*numFrame, j), k, i*latentDim + j);
  
  //cout << m_gp_y << endl;
  
  
  
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //	GP module construction
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //	Create Kernel
  m_cmpndKern = new CCmpndKern(m_gp_x);
  //m_kernels.push_back(new CRbfardKern(m_gp_x));
  m_kernels.push_back(new CRbfKern(m_gp_x));
  m_kernels.push_back(new CBiasKern(m_gp_x));
  m_kernels.push_back(new CWhiteKern(m_gp_x));
  for (unsigned i = 0; i < m_kernels.size(); i++)
    m_cmpndKern->addKern(m_kernels[i]);
  
  //	Create Noise
  m_noise = new CGaussianNoise(&m_gp_y);
  
  //	Init GP model
  m_GP = new CGp(m_gp_x.getCols(), m_gp_y.getCols(), &m_gp_x, &m_gp_y, m_cmpndKern, m_noise);
  m_GP->setBias(meanCol(m_gp_y));
  m_GP->setScale(stdCol(m_gp_y));
  m_GP->updateM();
  m_GP->setVerbosity(0);
  cout << "Learning GP ...";
  m_GP->optimise(1000);
  cout << "Done." << endl;
  
  //	GP regression test
  //CMatrix	X_mean(meanCol(m_gp_x));
  //CMatrix	X_1(X_mean);
  //for (int i = 0; i < X_1.getCols(); i++) X_1.setVal(m_gp_x.getVal(0, i), 0, i);
  //CMatrix	Y_mean(meanCol(m_gp_y));
  //CMatrix Y_pred(X_mean.getRows(), m_GP->getOutputDim());
  //m_GP->posteriorMean(Y_pred, X_mean);
  //
  //cout << "X_mean : " << endl << X_mean << endl << endl;
  //cout << "Y_mean : " << endl << Y_mean << endl << endl;
  //cout << "Y_pred : " << endl << Y_pred << endl << endl;
  //
  //m_GP->posteriorMean(Y_pred, X_1);
  //cout << "Y_pred : " << endl << Y_pred << endl << endl;
  // updateTrajectory();
}

GaitPatternGenerator::~GaitPatternGenerator()
{
  //	memory deallocation
  delete m_GP;
  delete m_cmpndKern;
  delete m_noise;
  
  for (unsigned i = 0; i < m_kernels.size(); i++)
    delete m_kernels[i];
}

void GaitPatternGenerator::setFeature(double _val, featureIndex _idx)
{
  m_features.setVal(_val, (unsigned)_idx);
  m_isTrajUptodate = false;
  //updateTrajectory();
}

void GaitPatternGenerator::setFeature(vector<double>& _vals)
{
  DIMENSIONMATCH(_vals.size() == m_features.getNumElements());
  m_features.fromArray(&_vals[0]);
  m_isTrajUptodate = false;
  //updateTrajectory();
}

void GaitPatternGenerator::setFeature(const CMatrix & _vals)
{
  m_features = _vals;
  m_isTrajUptodate = false;
  //updateTrajectory();
}

void GaitPatternGenerator::setClearance(double _z)
{
  m_clearance = _z;
  double dz = max(0.0, m_clearance - (m_collisionZtoe + m_ZoffsetPelvis));
  m_dq = _calc_dq(m_collisionPose, dz, m_colAvoidScheme);
  return;
}

void GaitPatternGenerator::enableCollisionAvoidance(bool _on)
{
  m_collisionHandling = _on;
}

void GaitPatternGenerator::setCollisionAvoidanceScheme(MODIFICATION_METHOD _scheme)
{
	m_colAvoidScheme = _scheme;
	setClearance(m_clearance);
}

void GaitPatternGenerator::updateTrajectory()
{
  m_isTrajUptodate = true;
  //	x0 is initial latent coordinates which are estimated by using GP.
  CMatrix x0(1, m_GP->getOutputDim());
  m_GP->posteriorMean(x0, m_features);
  
  //	X_init is just reshaped x0.
  CMatrix X_init(m_GPDM.getDynOrder(), m_GPDM.m_latentDim);
  for (int i = 0; i < x0.getNumElements(); i++)
    X_init.setVal(x0.getVal(i), i / m_GPDM.m_latentDim, i%m_GPDM.m_latentDim);
  
  //	Simulate dynamics
  CMatrix X_sim = m_GPDM.simulateDynamics(X_init, m_GPDM.m_numFrame - m_GPDM.m_numDuplicatedFrame);
  
  //	Append first row to end
  CMatrix X_full(X_sim.getRows() + 1, X_sim.getCols());
  X_full.setMatrix(0, 0, X_sim);
  X_full.copyRowRow(X_full.getRows() - 1, X_init, 0);
  
  //debug
  //cout << X_full << endl <<endl;
  //
  
  //	reconstruction in ovservation space
  CMatrix Y_full = m_GPDM.mapping(X_full);

  //debug
  //cout << Y_full << endl <<endl;
  //
  
  //	make spline
  vector<point> dataPts;
  dataPts.reserve(Y_full.getRows());
  vector<double> Y_single_row(Y_full.getCols() + 1);
  for (int row = 0; row < Y_full.getRows(); row++)
  {
    Y_single_row[0] = (double)(row) / (Y_full.getRows() - 1);
    for (int col = 0; col < Y_full.getCols(); col++)
      Y_single_row[col + 1] = Y_full.getVal(row, col);
    dataPts.push_back(point(Y_single_row.size(), &Y_single_row[0]));
  }
  
  m_trajSpline = cubic_spline(dataPts,
                              cubic_spline::end_condition::periodic,
                              cubic_spline::parametrization::function_spline);

  auto initPt = m_trajSpline.evaluate(0);

  _updateCollisionInfo(m_trajSpline);
}

void GaitPatternGenerator::exportTraj(const string & fileName, double timeStep)
{
  ofstream outText(fileName);
  outText.precision(4);
  vector<double> pose;
  int row = 0;
  for (double t = 0; t <= 1; t += timeStep, row++)
  {
    pose = queryPose(t);
    for (size_t col = 0; col < pose.size(); col++)
      outText << pose[col] << '\t';
    outText << '\n';
  }
  outText.close();
}

vector<double> GaitPatternGenerator::queryPose(double _t, bool isTimeNormalized)
{
  if (!m_isTrajUptodate)
    updateTrajectory();
  
  vector<double> pose((unsigned)JOINT_LABEL::NUM_OF_JOINTS);
  auto pose_right = m_trajSpline.evaluate(fmod(_t, 1));
  auto pose_left = m_trajSpline.evaluate(fmod(_t + 0.5, 1));
  
  if (m_collisionHandling)
  {
    auto dq_R = _query_dq(fmod(_t, 1));
    auto dq_L = _query_dq(fmod(_t + 0.5, 1));
    for (int label = (unsigned)JOINT_LABEL::PELVIS_X; label != (unsigned)JOINT_LABEL::L_HIP_ROLL; label++)
    {
      pose_right(label + 2) += dq_R[label];
      pose_left(label + 2) += dq_L[label];
    }
  }
  
  for (int label = (unsigned)JOINT_LABEL::PELVIS_X; label != (unsigned)JOINT_LABEL::L_HIP_ROLL; label++)
    pose[label] = pose_right(label + 2);
  for (int label = (unsigned)JOINT_LABEL::L_HIP_ROLL; label != (unsigned)JOINT_LABEL::NUM_OF_JOINTS; label++)
    pose[label] = pose_left(label - ((unsigned)JOINT_LABEL::L_HIP_ROLL - (unsigned)JOINT_LABEL::R_HIP_ROLL) + 2);
  
  return pose;
}

vecSE3 GaitPatternGenerator::_forwardKinematics(const vec & _conf) const
{
  double frontFootRatio = 0.7;
  vecSE3	markerSE3;
  //	both leg
  if (_conf.size() == (unsigned)JOINT_LABEL::NUM_OF_JOINTS)
    markerSE3.resize((unsigned)MARKER_LABEL::NUM_OF_MARKERS);
  //	only right leg
  else if (_conf.size() == (unsigned)JOINT_LABEL::L_HIP_ROLL)
    markerSE3.resize((unsigned)MARKER_LABEL::L_HIP);
  else
    return markerSE3;
  
  //	forward kinematics routine for right leg.
  double anklePitchOffset = 5;
  markerSE3[(unsigned)MARKER_LABEL::PELVIS] =
		EulerZYX(Vec3(_conf[(unsigned)JOINT_LABEL::PELVIS_YAW], 0, 0) * SR_RADIAN, Vec3(_conf[(unsigned)JOINT_LABEL::PELVIS_X], _conf[(unsigned)JOINT_LABEL::PELVIS_Y], _conf[(unsigned)JOINT_LABEL::PELVIS_Z]) / 10.0);
  
  markerSE3[(unsigned)MARKER_LABEL::R_HIP] =
		markerSE3[(unsigned)MARKER_LABEL::PELVIS] *
		EulerZYX(Vec3(_conf[(unsigned)JOINT_LABEL::R_HIP_YAW], _conf[(unsigned)JOINT_LABEL::R_HIP_PITCH], _conf[(unsigned)JOINT_LABEL::R_HIP_ROLL]) * SR_RADIAN, Vec3(0, -m_features.getVal((unsigned)featureIndex::BI_TROCH) / 2, 0));
  
  markerSE3[(unsigned)MARKER_LABEL::R_KNEE] =
		markerSE3[(unsigned)MARKER_LABEL::R_HIP] *
		EulerZYX(Vec3(0, _conf[(unsigned)JOINT_LABEL::R_KNEE_PITCH], 0) * SR_RADIAN, Vec3(0, 0, -m_features.getVal((unsigned)featureIndex::THIGH)));
  
  markerSE3[(unsigned)MARKER_LABEL::R_ANKLE] =
		markerSE3[(unsigned)MARKER_LABEL::R_KNEE] *
		EulerZYX(Vec3(0, _conf[(unsigned)JOINT_LABEL::R_ANKLE_PITCH] + anklePitchOffset, 0) * SR_RADIAN, Vec3(0, 0, -m_features.getVal((unsigned)featureIndex::CALF)));
  
  markerSE3[(unsigned)MARKER_LABEL::R_TOE] =
		markerSE3[(unsigned)MARKER_LABEL::R_ANKLE] *
		EulerZYX(Vec3(0, 0, 0) * SR_RADIAN, Vec3(m_features.getVal((unsigned)featureIndex::FOOT_L) * frontFootRatio, 0, -m_features.getVal((unsigned)featureIndex::MALLEOLUS_H)));
  
  markerSE3[(unsigned)MARKER_LABEL::R_HEEL] =
		markerSE3[(unsigned)MARKER_LABEL::R_ANKLE] *
		EulerZYX(Vec3(0, 0, 0) * SR_RADIAN, Vec3(-m_features.getVal((unsigned)featureIndex::FOOT_L) * (1 - frontFootRatio), 0, -m_features.getVal((unsigned)featureIndex::MALLEOLUS_H)));
  
  if (_conf.size() == (unsigned)JOINT_LABEL::L_HIP_ROLL)
    return markerSE3;
  
  //	forward kinematics routine for left leg.
  
  markerSE3[(unsigned)MARKER_LABEL::L_HIP] =
		markerSE3[(unsigned)MARKER_LABEL::PELVIS] *
		EulerZYX(Vec3(- _conf[(unsigned)JOINT_LABEL::L_HIP_YAW], _conf[(unsigned)JOINT_LABEL::L_HIP_PITCH], - _conf[(unsigned)JOINT_LABEL::L_HIP_ROLL]) * SR_RADIAN, Vec3(0, m_features.getVal((unsigned)featureIndex::BI_TROCH) / 2, 0));
  
  markerSE3[(unsigned)MARKER_LABEL::L_KNEE] =
		markerSE3[(unsigned)MARKER_LABEL::L_HIP] *
		EulerZYX(Vec3(0, _conf[(unsigned)JOINT_LABEL::L_KNEE_PITCH], 0) * SR_RADIAN, Vec3(0, 0, -m_features.getVal((unsigned)featureIndex::THIGH)));
  
  markerSE3[(unsigned)MARKER_LABEL::L_ANKLE] =
		markerSE3[(unsigned)MARKER_LABEL::L_KNEE] *
		EulerZYX(Vec3(0, _conf[(unsigned)JOINT_LABEL::L_ANKLE_PITCH] + anklePitchOffset, 0) * SR_RADIAN, Vec3(0, 0, -m_features.getVal((unsigned)featureIndex::CALF)));
  
  markerSE3[(unsigned)MARKER_LABEL::L_TOE] =
		markerSE3[(unsigned)MARKER_LABEL::L_ANKLE] *
		EulerZYX(Vec3(0, 0, 0) * SR_RADIAN, Vec3(m_features.getVal((unsigned)featureIndex::FOOT_L) * frontFootRatio, 0, -m_features.getVal((unsigned)featureIndex::MALLEOLUS_H)));
  
  markerSE3[(unsigned)MARKER_LABEL::L_HEEL] =
		markerSE3[(unsigned)MARKER_LABEL::L_ANKLE] *
		EulerZYX(Vec3(0, 0, 0) * SR_RADIAN, Vec3(-m_features.getVal((unsigned)featureIndex::FOOT_L) * (1 - frontFootRatio), 0, -m_features.getVal((unsigned)featureIndex::MALLEOLUS_H)));
  
  return markerSE3;
}

void GaitPatternGenerator::_updateCollisionInfo(const cagd::cubic_spline & _traj)
{
  double minZ_toe = 1e+7;
  double minX_toe = 1e+7;
  double minZ_ank = 1e+7;
  double minPhase = -1;		//	phase in normalized time when z-axis of toe is minimized.
  double resolution = 0.01;	//	resolution in normalized time for inspecting trajectory cycle.
  
  vec q(_traj.dim() - 1);
  for (double s = 0.0; s < 0.33; s += resolution)
  {
    point pt = _traj.evaluate(s);
    q.assign(pt._elem.begin() + 1, pt._elem.end());
    q[(unsigned)JOINT_LABEL::PELVIS_YAW] = 0.0;
    q[(unsigned)JOINT_LABEL::R_HIP_ROLL] = 0.0;
    q[(unsigned)JOINT_LABEL::R_HIP_YAW] = 0.0;
    vecSE3 kinResult = _forwardKinematics(q);
    
	if (abs(kinResult[(unsigned)MARKER_LABEL::R_TOE].GetPosition()[0]) < minX_toe)
	{
		minX_toe = abs(kinResult[(unsigned)MARKER_LABEL::R_TOE].GetPosition()[0]);
	}
	else
	{  
      minZ_toe = kinResult[(unsigned)MARKER_LABEL::R_TOE].GetPosition()[2];
      minPhase = s;
      m_collisionPose = kinResult;
      m_collisionConf = q;
    }
    
    if (kinResult[(unsigned)MARKER_LABEL::R_ANKLE].GetPosition()[2] < minZ_ank)
      minZ_ank = kinResult[(unsigned)MARKER_LABEL::R_ANKLE].GetPosition()[2];
  }
  
  m_ZoffsetPelvis = -minZ_ank + m_features.getVal((unsigned)featureIndex::MALLEOLUS_H);
  m_collisionPhase = minPhase;
  m_collisionZtoe = minZ_toe;
  
  setClearance(m_clearance);
  return;
}

vec GaitPatternGenerator::_query_dq(double _s) const
{
  double mod_width = 0.3;
  if (_s > m_collisionPhase + mod_width / 2 || _s < m_collisionPhase - mod_width / 2)
    return m_dq * 0.0;
  
  //	else
  double factor = cos(SR_TWO_PI / mod_width * (_s - m_collisionPhase)) / 2 + 0.5;
  
  return m_dq * factor;
}

vec GaitPatternGenerator::_calc_dq(const vecSE3 & _collisionPose, double _dz, MODIFICATION_METHOD _mode)
{
  auto dbg_pose = _forwardKinematics(m_collisionConf);
  Vec3 v1 = _collisionPose[(unsigned)MARKER_LABEL::R_KNEE].GetPosition() - _collisionPose[(unsigned)MARKER_LABEL::R_HIP].GetPosition();
  Vec3 v2 = _collisionPose[(unsigned)MARKER_LABEL::R_TOE].GetPosition() - _collisionPose[(unsigned)MARKER_LABEL::R_KNEE].GetPosition();
  Vec3 v3 = _collisionPose[(unsigned)MARKER_LABEL::R_ANKLE].GetPosition() - _collisionPose[(unsigned)MARKER_LABEL::R_KNEE].GetPosition();
  Vec3 v4 = _collisionPose[(unsigned)MARKER_LABEL::R_ANKLE].GetPosition() - _collisionPose[(unsigned)MARKER_LABEL::R_HIP].GetPosition();
  Vec3 v5 = _collisionPose[(unsigned)MARKER_LABEL::R_TOE].GetPosition() - _collisionPose[(unsigned)MARKER_LABEL::R_HIP].GetPosition();
  
  double l1 = Norm(v1);
  double l2 = Norm(v2);
  double l3 = Norm(v3);
  
  vec	sol, dq((unsigned)JOINT_LABEL::L_HIP_ROLL);
  for (size_t i = 0; i < dq.size(); i++)
    dq[i] = 0.0;
  
  double theta0 = 0.0;
  switch (_mode)
  {
  case MODIFICATION_METHOD::HOLD_ANKLE_ANGLE:
      theta0 = acos(Inner(v2, v3) / (l2*l3));
      sol = _inverseKinematics(v5[0], v5[2] + _dz, l1, l2);
      sol[1] += theta0 * SR_DEGREE;
      dq[(unsigned)JOINT_LABEL::R_HIP_PITCH] = sol[0] - m_collisionConf[(unsigned)JOINT_LABEL::R_HIP_PITCH];
      dq[(unsigned)JOINT_LABEL::R_KNEE_PITCH] = sol[1] - m_collisionConf[(unsigned)JOINT_LABEL::R_KNEE_PITCH];
      break;
      
  case MODIFICATION_METHOD::HOLD_INCIDENCE_ANGLE:
      sol = _inverseKinematics(v4[0], v4[2] + _dz, l1, l3);
      dq[(unsigned)JOINT_LABEL::R_HIP_PITCH] = sol[0] - m_collisionConf[(unsigned)JOINT_LABEL::R_HIP_PITCH];
      dq[(unsigned)JOINT_LABEL::R_KNEE_PITCH] = sol[1] - m_collisionConf[(unsigned)JOINT_LABEL::R_KNEE_PITCH];
      dq[(unsigned)JOINT_LABEL::R_ANKLE_PITCH] = -(dq[(unsigned)JOINT_LABEL::R_HIP_PITCH] + dq[(unsigned)JOINT_LABEL::R_KNEE_PITCH]);
      break;
      
    default:
      //	you chose wrong option.
      break;
  }
  
  return dq;
}
vec GaitPatternGenerator::_inverseKinematics(double _px, double _pz, double _l1, double _l2)
{
  vec sol(2);
  double cosine = (_px*_px + _pz*_pz - _l1*_l1 - _l2*_l2) / (2 * _l1*_l2);
  double theta2 = abs(atan2(sqrt(1 - cosine*cosine), cosine));
  double thetaC = atan2(-_px, -_pz);
  double alpha = asin(_l2 / sqrt(_px*_px + _pz*_pz) * sin(theta2));
  
  //	sol. 1
  sol[0] = thetaC - alpha;
  sol[1] = theta2;
  
  //	sol. 2, deprecated
  //sol[0] = thetaC + alpha;
  //sol[1] = -theta2;
  
  return sol * SR_DEGREE;
}

